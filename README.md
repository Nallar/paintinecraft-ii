# PaintineCraft II

<div align="center" width="100%">
    <img src="profileImage/PaintineCraft%20II.png" 
        alt= “PaintineCraftII” 
        width="200" height="200">
</div>


# Installation 

Clonez le dépôt ou téléchargez-le depuis GitLab.

Choisir parmi les trois méthodes d'installation :

## Installation Launcher Minecraft Officiel

- Installez le launcher Officiel Minecraft :
 https://www.minecraft.net/fr-fr/download  

 - Double-cliquez sur `install.bat` pour créer un nouveau profil `PaintineCraft II`  

 - Lancer le profil depuis le launcher minecraft comme en Vanilla

- Le dossier courant est le nouveau dossier "gamedir" pour le profil du launcher, **vous ne devez donc PAS bouger ce dossier** (ou modifier le gamedir manuellement)

> :warning: Pour que le script `bat` fonctionne il ne faut pas que vous ayez custom votre chemin `.minecraft`. Vous pouvez éditer le chemin dans le script si vous voulez.

> :warning: Quand même, checkez les sources des scripts `bat/ps1` que vous lancez, même si c'est mien, on est jamais trop prudent.

## Installation **Manuelle** via le launcher Minecraft Officiel

- Installez le launcher Officiel Minecraft :
 https://www.minecraft.net/fr-fr/download  

 - Double-cliquez sur l'installeur fabric (fichier `fabric-installer.jar` à executer avec votre Java)

 - Installez `fabric` avec les paramètres suivants:
<div  align="center" width="100%">
    <img src="images/tuto install fabric.png" 
        alt= “PaintineCraftII” 
        width="400" height="200">
</div>

- Ouvrez votre le launcher minecraft offi puis suivez les étapes suivantes :
1.
<div  align="center" width="100%">
    <img src="images/tuto install manuelle 1.png" 
        alt= “PaintineCraftII” >
</div>
2.
<div  align="center" width="100%">
    <img src="images/tuto install manuelle 2.png" 
        alt= “PaintineCraftII” >
</div>
3.
<div align="center" width="100%">
    <img src="images/tuto install manuelle 3.png" 
        alt= “PaintineCraftII” >
</div>

- Le repertoire du jeu doit être la racine du dossier git (là où se trouve le dossier `mods`), vous pouvez déplacer ce dossier mais il faudra mettre à jour le répertoire du jeu pour que le profile fonctionne, sinon il se lancera en vanilla.

- Lancer le jeu comme à votre habitude

> :information_source: Vous pouvez faire une clean install en supprimant le profile du launcher, et en reéxecutant ce tuto.

 ## Installation Curseforge

 - Installez Curseforge (sans overworlf, fuck overworlf) : https://download.curseforge.com/#download-options (bien choisir la version `Standalone` sans overworlf (sauf si vous voulez overwolf avec évidemment))

 Dans la partie Minecraft :
 - Cliquez `+ Create Custom Profile`
 - Cliquez `Import`
 - Selectionnez le fichier .Zip `PaintineCraft II - CurseForge Export.zip`

 # FAQ
 ## Est-ce que c'est encore du moddé de merde ?
Oui, MAIS ça n'a rien à voir avec le premier PaintineCraft. Il s'agit de Vanilla avec un accent mis sur le fait de ne pas dénaturer le gameplay Vanilla.

 ## Est-ce que ça prend 3 plombes à lancer comme PaintineCraft I ?
 Non ! C'est plus Forge le modloader, mais Fabric, ça se lance, à peu de chose pret, aussi vite que Vanilla ! (sauf le premier lancement)

 ## Quelle version?
On est sur du 1.19.2 

 ## Y a quoi comme mods ?
 Concretement il n'y a que 5 "gros" mods/ groupes de mods qui modifient un peu le jeu (ne faites pas attention aux "200 mods" de fabric, ce sont des libs ou des mods de performance):  
- `Artificats`, mod d'exploration qui rajoutent des petits objets avec des bonus sympas pour recompenser l'exploration
- `Chipped` et `Supplementaries`, qui ajoute BEAUCOUP BEAUCOUP de décoration
- `Create`, qui permet d'ajouter des machines qui fonctionnent avec de la redstone et des moulins (à vent/eau) et des trains. Je precise qu'il n'a rien a voir avec les mods "technologiques" de PaintineCraft I, celui-ci est très rustique et s'intègre très bien à Vanilla. Il est également accompagné de tutoriel in-game pour ceux qui veulent s'y interesser. Il est bien sûr très optionnel.
- `Farmer's Delight`, une extension de la bouffe de minecraft (plein de nouveaux types de plats)
- Le pack de `YUNG's` (better mineshaft, dungeons etc), qui permet d'améliorer les structures de minecraft de base comme les mineshaft, les donjons ou encore les temples.  

En bref, on a très peu de mods, et ils sont tous vanilla friendly.

## Y-a t il les shaders de dispo ?
Pour ceux qui connaissent pas ça permet de rendre le jeu plus beau (Minecraft RTX tmtc), c'est dispo via Iris et Sodium, et dans le pack de base j'ai mis les Complementary Shaders qui vous pouvez custom selon vos envies.
Je précise que les shaders sont OPTIONNELS, ça plait pas à tout le monde, et y a des impacts sur les perfs.

## Qui-qui paie ?
Actuellement ça tourne sur mon serveur oracle que je ne paye pas, donc c'est gratuit. On est pas à l'abri que les performances ne soient pas folles, et qu'il faille upgrade plus tard.

## Est-ce que vous pouvez proposer de rajouter des mods ?
Alors oui et non, le but c'est de rester léger cette fois. Cependant, j'ai créé ce dépôt Git pour que ce soit upgradable plus facilement, donc à voir.

## Est ce que vous serez OP ?
Oui, si vous le demandez.

## C'est quoi l'IP ?
Pas ici, c'est un repo public !

