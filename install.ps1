﻿Set-Location -LiteralPath $PSScriptRoot

$mc_version = "1.19.2"
$profile_name = "PaintineCraft II"

$mc_path = "$($env:APPDATA)\.minecraft"
Write-Output "Chemin .minecraft : "
Write-Output $mc_path

$accent_regex = '[éèêëàâäôöûüù]'
$game_dir_path = Resolve-Path "." | Select-Object -ExpandProperty Path
$game_dir_path = [System.Text.RegularExpressions.Regex]::Escape($game_dir_path)
$game_dir_path = [System.Text.RegularExpressions.Regex]::Replace($game_dir_path, $accent_regex, {param($match) "\u{0:x4}" -f [int][char]$match.Value})

Write-Output "Chemin courant : "
Write-Output $game_dir_path

java.exe -jar fabric-installer.jar client -dir $mc_path -mcversion $mc_version

Write-Output "Fabric Installed"
Write-Output "Updating JSON profiles"

$profile_json_path = "$($mc_path)\launcher_profiles.json"
$profile_json = Get-Content $profile_json_path | ConvertFrom-Json

$profile_fabric = $profile_json.profiles."fabric-loader-1.19.2"
$profile_fabric | add-member -Name "gameDir" -Value $game_dir_path -MemberType NoteProperty
$profile_fabric."name" = $profile_name

$profile_json | ConvertTo-Json -depth 3| % { [System.Text.RegularExpressions.Regex]::Unescape($_) } | set-content $profile_json_path
Write-Output "Finished updating JSON profiles"

Read-Host -Prompt "Press Enter to exit"