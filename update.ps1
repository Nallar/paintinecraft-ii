﻿Set-Location -LiteralPath $PSScriptRoot

# Téléchargement du fichier ZIP
$url = "https://gitlab.com/Nallar/paintinecraft-ii/-/archive/main/paintinecraft-ii-main.zip?path=mods"
$outputPath = "./mods.zip"
$outputPathExtracted = "./modsExtracted"
$outputPathExtractedComplet = "./modsExtracted/paintinecraft-ii-main-mods/mods"
$modPath = "./mods/"
$modBackupPath = "./modsBackUp/"
# Invoke-WebRequest $url -OutFile $outputPath

if (Test-Path $modBackupPath) {
    Remove-Item $modBackupPath
}

if (Test-Path $outputPathExtractedComplet) {
    Remove-Item $outputPathExtractedComplet
}

# Renommage du dossier "mods" en "modsBackUp" s'il existe
if (Test-Path $modPath) {
    Rename-Item $modPath -NewName "modsBackUp"
}


# Extraction du contenu du ZIP dans le dossier "mods"
Add-Type -AssemblyName System.IO.Compression.FileSystem
[System.IO.Compression.ZipFile]::ExtractToDirectory($outputPath, $outputPathExtracted)

Move-Item -Path $outputPathExtractedComplet -Destination "."

Remove-Item $outputPathExtracted

Read-Host -Prompt "Press Enter to exit"